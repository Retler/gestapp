package com.example.sergej.gest_an_app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.provider.AlarmClock;
import android.util.Log;
import android.widget.Toast;

import java.util.Map;

/*
 * Made by Lasse
 * This app handles the different ways to launch applications and actions when a gesture is drawn. It is called when a gesture is recognized
 */
public class Launcher {
    private Context context;
    private static final String TAG = "launcher";
    private boolean hasFlash;
    private boolean flashToggled;
    private static Map<String, String> appLaunchList;
    private String gestureAction;

    public Launcher(Context givenContext, String gestureAction){

        this.gestureAction = gestureAction;
        appLaunchList = MainActivity.getLaunchList();

        context = givenContext;

        hasFlash = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        flashToggled = false;
        // Executing actions:

    }

    //We check the actions individually as they have different ways of launching
    public void launch(){
        //first we check if an app/action is chosen for the drawn gesture. id not, a toast will serve as feedback
        if (gestureAction.equals(Constants.DEFAULT_ACTION)){
            Toast.makeText(context, "No app is chosen for this gesture, please go to settings and choose an app", Toast.LENGTH_LONG).show();
            return;
        }
        // Check what gesture has been performed and do the desired action
        if (gestureAction.equals(context.getString(R.string.open_alarm))) {
            openAlarm();
            return;
        }
        if (gestureAction.equals(context.getString(R.string.open_calendar))) {
            openCalendar();
            return;
        }
        if (gestureAction.equals(context.getString(R.string.open_camera))) {
            openCamera();
            return;
        }
        if (gestureAction.equals(context.getString(R.string.toggle_flashlight))) {
            toggleFlashlight();
            return;
        }
        if (gestureAction.equals(context.getString(R.string.open_maps))) {
            openMaps();
            return;
        }
        //if neither an action nor no app is chosen, the only other option is an user-installed app
        //They all launch the same way
        launchApp(appLaunchList.get(gestureAction));
    }

    private void openAlarm() {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
        context.startActivity(intent);
        Log.d(TAG, "open alarm activated!");
    }

    private void openCalendar() {
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        context.startActivity(intent);
        Log.d(TAG, "open calendar activated!");
    }

    private void openCamera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        context.startActivity(intent);
        Log.d(TAG, "open camera activated!");
    }

    private void openMaps() {
        Uri location = Uri.parse("geo:0,0?q=Storcenter+Nord,Aarhus+N,Denmark");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
        context.startActivity(mapIntent);
    }

    private void toggleFlashlight(){
        // If the device has a flashlight make a transparent camera instance and instanciate the
        // torch for this instance
        if (hasFlash) {
            CameraManager cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
            String cameraId = null;
            try {
                cameraId = cameraManager.getCameraIdList()[0];
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            // check the state of the torch and turn it off or on
            if (!flashToggled) {
                try {
                    cameraManager.setTorchMode(cameraId, true);
                    flashToggled = true;
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    cameraManager.setTorchMode(cameraId, false);
                    flashToggled = false;
                } catch (CameraAccessException e1) {
                    e1.printStackTrace();
                }
            }
            // if the device doesn't have a flashlight
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Sorry, your device doesn't support flash light!")
                    .setTitle("Error")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
            builder.show();
        }
        Log.d(TAG, "open flashlight activated!");
    }

    private void launchApp(String packageName){
        // If the desired action is to open an app from the apps list, get this packagename
        // and open this application, if the application can be found on the device
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "This app no longer exist", Toast.LENGTH_SHORT).show();
        }
    }
}
