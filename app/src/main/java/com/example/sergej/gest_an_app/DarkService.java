package com.example.sergej.gest_an_app;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

/**
 * Written by Oliver
 */
public class DarkService extends Service implements SensorEventListener {

    protected final String TAG = "DarkService";
    private SensorManager mSensorManager;
    private Sensor mLightSensor;
    private PowerManager.WakeLock mWakeLock;
    private float mPreviousLux = 0.0f;
    private float mLux = 0.0f;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Start listening to the light sensor
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mSensorManager.registerListener(this, mLightSensor, SensorManager.SENSOR_DELAY_NORMAL);

        // Acquire wakelock
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DarkWakeLock");
        mWakeLock.acquire();

        // Service shouldn't shut down with app termination
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "in onDestroy");
        // Stop listening
        mSensorManager.unregisterListener(this, mLightSensor);

        // Release the taken wakelock
        if (mWakeLock != null) {
            mWakeLock.release();
        }
        // Stop the shake service
        stopService(new Intent(getApplicationContext(), ShakeGestureService.class));
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // If sensorEvent = Light, get the value of the light sensor
        if (sensorEvent.sensor.getType() == Sensor.TYPE_LIGHT) {
            mLux = sensorEvent.values[0];
        }

        // If light changes to 0 stop shake service, if it changes from 0 start the service
        if (mLux == 0 && mPreviousLux != 0) {
            Log.d(TAG, "onSensorChanged: was stopped");
            stopService(new Intent(getApplicationContext(), ShakeGestureService.class));
        } else if (mLux != 0 && mPreviousLux == 0) {
            Log.d(TAG, "onSensorChanged: was started");
            startService(new Intent(getApplicationContext(), ShakeGestureService.class));
        }

        mPreviousLux = mLux;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    // Can't bind to this service
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
