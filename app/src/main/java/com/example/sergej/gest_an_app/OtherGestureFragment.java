package com.example.sergej.gest_an_app;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Implemented by all
 */
public class OtherGestureFragment extends Fragment {
    public static OtherGestureFragment newInstance() {
        // Create the OtherGestureFragment with these parameters and return it to the viewPager
        OtherGestureFragment fragment = new OtherGestureFragment();
        Bundle args = new Bundle();
        args.putInt("otherInt", 1);
        args.putString("otherTitle", "Other Gestures");

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.other_fragment, container, false);

        ListView lv = view.findViewById(R.id.shake_gestures);

        // get the list of all touch gestures and write it in the fragment
        ArrayList<String> shakeGestures = new ArrayList<>();
        shakeGestures.add(getString(R.string.gesture_shake));

        // Add these to the ListAdapter which places em in the view
        ShakeGestureListAdapter adapter = new ShakeGestureListAdapter(getContext(), R.layout.other_fragment_row, shakeGestures);
        for(String gesture : shakeGestures){
            adapter.add(gesture);
        }
        lv.setAdapter(adapter);

        // return this fragment view
        return view;
    }

    }

