package com.example.sergej.gest_an_app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
/**
 * Written by Sergej'
 * The shakeGestureListAdapter and TouchGestureListAdapter are very similar, but not identical.
 */
public class ShakeGestureListAdapter extends ArrayAdapter<String> {
    private int layoutResourceId;
    private Context context;
    private ArrayList<String> shakeGestures;

    private static final String TAG = "LISTADAPTER";
    private GlobalState globalState;
    private String[] actionArray;

    private String[] appList;

    public ShakeGestureListAdapter(@NonNull Context context, int resource, ArrayList<String> gestures) {
        super(context, resource);
        this.shakeGestures = gestures;
        this.layoutResourceId = resource;
        this.context = context;

        appList = MainActivity.getAppList();
    }

        //The getView works sorta like a onCreate from a regular activity
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        //We inflate the shake layout so we can add shake gestures to it
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        TextView gestureName = row.findViewById(R.id.shake_action_name);
        TextView gestureDescription = row.findViewById(R.id.shake_action_description);
        String gestureId = shakeGestures.get(position);

        ShakeGestureHolder holder = new ShakeGestureHolder();

        holder.gestureId = gestureId;
        holder.gestureName = gestureName;
        holder.gestureDescription = gestureDescription;

        setupHolder(holder);
        row.setTag(holder);


        globalState = (GlobalState) ((Activity) context).getApplication();
        actionArray = context.getResources().getStringArray(R.array.action_array);
        LinearLayout parentLayout = row.findViewById(R.id.linearLayoutForAction);

        gestureDescription.setText(globalState.getActionForGesture(gestureId));
        //makes each row clickable (the layout is set to clickable in the .xml associated to the layout inflated
        parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeDialog(gestureDescription, holder);
            }
        });

        return row;
    }


    //Holder for the name, description and id, to eliminate typos and make it easier to work with
    private void setupHolder(ShakeGestureHolder holder) {
        holder.gestureName.setText(holder.gestureId);
        holder.gestureDescription.setText("Opens " + getGestureAction(holder.gestureId));
    }

    private String getGestureAction(String gesture) {
        GlobalState globalState = (GlobalState) context.getApplicationContext();
        String action = globalState.getActionForGesture(gesture);
        return action;
    }

    public static class ShakeGestureHolder{
        String gestureId;
        TextView gestureName;
        TextView gestureDescription;

    }
    //Makes the dialog for when the gesture is pressed in settings
    private void makeDialog(TextView textView, ShakeGestureHolder holder) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Actions")
                .setItems(R.array.action_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        if (actionArray[which].equals(Constants.APPS_CHOOSER)){
                            dialogForAllApps(holder, textView);
                            return;
                        }
                        String chosenAction = actionArray[which];
                        globalState.storeActionForGesture(holder.gestureId, chosenAction);
                        textView.setText(chosenAction);
                    }
                });

        builder.show();
    }
    //the more apps.. dialog, made into a separate method for prettier code.
    private void dialogForAllApps(ShakeGestureHolder holder, TextView textView) {
        AlertDialog.Builder appBuilder = new AlertDialog.Builder(context);
        appBuilder.setTitle("Installed Apps")
                .setItems(appList, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String chosenApp = appList[i];
                        globalState.storeActionForGesture(holder.gestureId, chosenApp);
                        textView.setText(chosenApp);
                    }
                });
        appBuilder.show();
    }
}
