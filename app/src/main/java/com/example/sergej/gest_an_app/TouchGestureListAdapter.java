package com.example.sergej.gest_an_app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Written by Sergej
 * The TouchGestureListAdapter and ShakeGestureListAdapter are very similar, but not identical.
 */
public class TouchGestureListAdapter extends ArrayAdapter<String> {
    private int layoutResourceId;
    private Context context;
    private ArrayList<String> touchGestures;

    private static final String TAG = "LISTADAPTER";
    private GlobalState globalState;
    private String[] actionArray;

    private String[] appList;



    public TouchGestureListAdapter(@NonNull Context context, int resource, ArrayList<String> gestures) {
        super(context, resource);
        this.touchGestures = gestures;
        this.layoutResourceId = resource;
        this.context = context;

        appList = MainActivity.getAppList();
    }
    //The getView works sorta like a onCreate from a regular activity
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        //We inflate the shake layout so we can add shake gestures to it
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        ImageView gestureIcon = row.findViewById(R.id.gesture_image);
        TextView gestureActionText = row.findViewById(R.id.gesture_action_text);

        String gesture = touchGestures.get(position);

        TouchGestureHolder holder = new TouchGestureHolder();
        holder.gesture = gesture;
        holder.gestureIcon = gestureIcon;
        holder.gestureActionText = gestureActionText;

        setupHolder(holder);
        row.setTag(holder);

        globalState = (GlobalState) ((Activity) context).getApplication();
        actionArray = context.getResources().getStringArray(R.array.action_array);
        LinearLayout parentLayout = row.findViewById(R.id.linearLayoutForAction);

        gestureActionText.setText(globalState.getActionForGesture(gesture));

        //makes each row clickable (the layout is set to clickable in the .xml associated to the layout inflated
        parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeDialog(gestureActionText, holder);

            }
        });

        return row;
    }

    //Makes the dialog for when the gesture is pressed in settings
    private void makeDialog(TextView textView, TouchGestureHolder holder) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Actions")
                .setItems(R.array.action_array, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        if (actionArray[which].equals(Constants.APPS_CHOOSER)){
                            dialogForAllApps(holder, textView);
                            return;
                        }
                        String chosenAction = actionArray[which];
                        globalState.storeActionForGesture(holder.gesture, chosenAction);
                        textView.setText(chosenAction);
                    }
                });

        builder.show();
    }
    //the more apps.. dialog, made into a separate method for prettier code.
    private void dialogForAllApps(TouchGestureHolder holder, TextView textView) {
        AlertDialog.Builder appBuilder = new AlertDialog.Builder(context);
        appBuilder.setTitle("Installed Apps")
                    .setItems(appList, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            String chosenApp = appList[i];
                            globalState.storeActionForGesture(holder.gesture, chosenApp);
                            textView.setText(chosenApp);
                        }
                    });
        appBuilder.show();
    }


    private void setupHolder(TouchGestureHolder holder) {
        holder.gestureIcon.setImageResource(getImageResource(holder.gesture));
        holder.gestureActionText.setText(getGestureAction(holder.gesture));
    }

    private String getGestureAction(String gesture) {
        GlobalState globalState = (GlobalState) context.getApplicationContext();
        String action = globalState.getActionForGesture(gesture);
        return action;
    }

    private int getImageResource(String gesture){
        if(gesture.equals(context.getString(R.string.down_arrow_gest))){
            return R.drawable.gesture_down;
        }
        if(gesture.equals(context.getString(R.string.left_arrow_gest))){
            return R.drawable.gesture_left;
        }
        if(gesture.equals(context.getString(R.string.right_arrow_gest))){
            return R.drawable.gesture_right;
        }
        if(gesture.equals(context.getString(R.string.lightning_gest))){
            return R.drawable.gesture_lightning;
        }
        if(gesture.equals(context.getString(R.string.circle_gest))){
            return R.drawable.gesture_circle;
        }
        return R.drawable.gesture_default;
    }

    public static class TouchGestureHolder{
        String gesture;
        ImageView gestureIcon;
        TextView gestureActionText;

    }
}
