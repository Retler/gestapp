package com.example.sergej.gest_an_app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * This class provides easy global access to shared preferences.
 * Made by Sergej
 */
public class GlobalState extends Application {
    //For storing actions to be remembered by each gesture it is associated with
    public void storeActionForGesture(String gesture, String action) {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.gesture_settings), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(gesture, action);
        editor.commit();
    }

    // retrieving gestures if any have been saved in previous use of app (is done every time the app opens)
    public String getActionForGesture(String gesture){
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.gesture_settings), Context.MODE_PRIVATE);
        String action = sharedPref.getString(gesture, Constants.DEFAULT_ACTION);
        return action;
    }

    //Store the chosen color of the gesture draw
    public void storeGestureColor(String color){
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.gesture_settings), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.gesture_color), color);
        editor.commit();
    }

    //Retrieve the chosen color of the gesture draw (is done every time the app opens)
    public String getGestureColor(){
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.gesture_settings), Context.MODE_PRIVATE);
        String color = sharedPref.getString(getString(R.string.gesture_color),"#"+Integer.toHexString(getColor(R.color.colorPrimary))); // Default color is primary
        return color;
    }
}
