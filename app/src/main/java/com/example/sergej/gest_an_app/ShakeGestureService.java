package com.example.sergej.gest_an_app;

import android.app.Service;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.widget.Toast;

/**
 * Written by Oliver, Lasse
 */
public class ShakeGestureService extends Service implements ShakeGesture.Listener {
    private static String TAG = "ShakeService";
    private ShakeGesture shakeGesture;

    @Override
    public void onCreate() {
        // Get the sensor manager and give the shakeGesture the sensor,
        // so it can listen to these events
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        shakeGesture = new ShakeGesture(this);
        shakeGesture.start(sensorManager);
    }

    @Override
    public void onDestroy() {
        shakeGesture.stop();
    }

    @Override
    public void hearShake() {
        // When shake is heard start the execution of the desired action
        executeActionForShake();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Implementation isn't needed
        return null;
    }

    private void executeActionForShake() {
        // Get the SharedPreferences and execute the action for the shake gesture
        GlobalState globalState = (GlobalState) getApplication();
        String action = globalState.getActionForGesture(getString(R.string.gesture_shake));

        Launcher launcher = new Launcher(this, action);

        launcher.launch();
    }
}