package com.example.sergej.gest_an_app;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.Prediction;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Written by Oliver, Sergej and Lasse
 */
public class MainActivity extends AppCompatActivity {
    private OnGesturePerformedListener handleGestureListener;
    private String TAG = "Main";
    private GestureLibrary gLib;
    private boolean testToggled;
    private String toggleToast;
    private GlobalState globalState;
    GestureOverlayView gestures;
    private View topMenu;

    private static ArrayList<String> appList;
    private static Map<String, String> appLaunchList;
    private Launcher launcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        testToggled = false;
        globalState = (GlobalState) getApplication();
        topMenu = findViewById(R.id.main_activity_menu);

        setupTopMenu();

        getPermissions();

        gLib = GestureLibraries.fromRawResource(this, R.raw.gesture);
        gLib.load();

        startService(new Intent(getApplicationContext(), DarkService.class));

        makeAppList();

        //Main functionality of the activity. Checks if a gesture is drawn, and proceeds to call the
        //launching of apps
        handleGestureListener = new OnGesturePerformedListener() {
            @Override
            public void onGesturePerformed(GestureOverlayView gestureView,
                                           Gesture gesture) {
                // try to recognize the gesture
                ArrayList<Prediction> predictions = gLib.recognize(gesture);

                // one prediction needed
                if (predictions.size() > 0) {
                    Prediction prediction = predictions.get(0);
                    // checking prediction
                    if (prediction.score > 1.0) {
                        // Gesture quality is OK - Execute proper action for a given gesture
                        executeActionForPrediction(prediction);
                    } else {
                        Toast.makeText(MainActivity.this, "Could not recognize gesture",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        gestures = findViewById(R.id.gestures);
        gestures.addOnGesturePerformedListener(handleGestureListener);
        gestures.setGestureStrokeAngleThreshold(90.0f);
    }

    //sets the top menu for toggling testmode and going to settings
    private void setupTopMenu() {
        Button menuTest = topMenu.findViewById(R.id.top_menu_testbutton);
        Button menuSettings = topMenu.findViewById(R.id.top_menu_settings);
        menuTest.setText("Toggle Test");
        menuSettings.setText("Settings");

        //toggles test mode
        menuTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                testToggled = !testToggled;
                toggleToast = testToggled ? "Test mode on" : "Test mode off";

                setGestureColor();

                Toast.makeText(MainActivity.this, toggleToast, Toast.LENGTH_LONG).show();
            }
        });

        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        //starts the settings activity
        menuSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(settingsIntent);
            }
        });
    }

    //opens apps
    private void executeActionForPrediction(Prediction prediction) {
        String gestureAction = globalState.getActionForGesture(prediction.name);
        launcher = new Launcher(MainActivity.this, gestureAction);
        //if test mode is toggled, make toast with which gesture is recognized. Dont open apps
        if (testToggled) {
            Toast.makeText(MainActivity.this, prediction.name + " recognized",
                    Toast.LENGTH_SHORT).show();
        } else {
            //When not in test mode, open apps
            launcher.launch();

        }
    }


    //Acquire needed permission. Each launched app will have their own permission check.
    // we need not check for all possible permissions
    private void getPermissions() {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 1);
        }
        if (checkSelfPermission(android.Manifest.permission.SET_ALARM) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.SET_ALARM}, 1);
        }
        if (checkSelfPermission(android.Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.READ_CALENDAR}, 1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setGestureColor();
    }

    private void setGestureColor(){
        int gestureColor =  Color.parseColor(globalState.getGestureColor());
        gestures.setGestureColor(gestureColor);
    }

    //Create a list of all installed apps that is not system apps.
    private void makeAppList(){
        //gets a list of all apps
        List<PackageInfo> packList = getPackageManager().getInstalledPackages(0);
        appList = new ArrayList<String>();
        appLaunchList = new HashMap<>();
        for (int i=0; i < packList.size(); i++)
        {
            PackageInfo packInfo = packList.get(i);
            //only take the ones installed by the user
            if (  (packInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0)
            {
                String appLaunchName = packInfo.packageName;
                String appName = packInfo.applicationInfo.loadLabel(getPackageManager()).toString();
                //Put name in one list for appDialog
                appList.add(appName);
                //Put name and launch path in map for Launcher
                appLaunchList.put(appName, appLaunchName);
            }
        }
    }

    //public method to retrieve the launchlist (used in launcher)
    public static Map<String, String> getLaunchList(){
        return appLaunchList;
    }

    //public method to make the arrayList of apps into an array. (used in appDialog)
    //appDialog needs arrays, cannot use arrayList.
    public static String[] getAppList(){
        String[] appArray = new String[appList.size()];
        appArray = appList.toArray(appArray);
        return appArray;
    }
}
