package com.example.sergej.gest_an_app;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Written by Oliver
 */
public class PagerAdapter extends FragmentPagerAdapter {
    private static int NUM_ITEMS = 2;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // This is how the viewPager determines what fragment to use when swiping
        switch (position) {
            case 0:
                return TouchGestureFragment.newInstance();
            case 1:
                return OtherGestureFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    //Simply changes the title of each page.
    public CharSequence getPageTitle(int position) {
        if (position == 0){
            return "Drawn Gestures";
        }
        if (position == 1){
            return "Shake gestures";
        }
        return "";
    }
}
