package com.example.sergej.gest_an_app;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Implemented by all
 */
public class TouchGestureFragment extends Fragment {
    private static final String TAG = "TouchGestureFragment";

    public static TouchGestureFragment newInstance() {
        // Create the TouchGestureFragment with these parameters and return it to the viewPager
        TouchGestureFragment fragment = new TouchGestureFragment();
        Bundle args = new Bundle();
        args.putInt("touchInt", 0);
        args.putString("touchTitle", "Touch Gestures");

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.touch_fragment, container, false);

        ListView lv = view.findViewById(R.id.touch_gestures);

        // get the list of all touch gestures and write it in the fragment
        ArrayList<String> touchGestures = new ArrayList<>();
        touchGestures.add(getString(R.string.lightning_gest));
        touchGestures.add(getString(R.string.right_arrow_gest));
        touchGestures.add(getString(R.string.left_arrow_gest));
        touchGestures.add(getString(R.string.down_arrow_gest));
        touchGestures.add(getString(R.string.circle_gest));

        // Add these to the ListAdapter which places em in the view
        TouchGestureListAdapter adapter = new TouchGestureListAdapter(getContext(), R.layout.touch_fragment_row, touchGestures);
        for(String gesture : touchGestures){
            adapter.add(gesture);
        }
        lv.setAdapter(adapter);

        // return this fragment view
        return view;
    }

}
