package com.example.sergej.gest_an_app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.util.ArrayList;
import java.util.Map;

/*
 * The settings activity serves as the settings tab.
 * made by lasse
 */
public class SettingsActivity extends AppCompatActivity {
    private String TAG = "Settings";
    private Context context;
    private GlobalState globalState;
    private Switch drawSwitch;
    private ViewPager viewPager;
    private EditText gestureHardwareShake;
    private View topMenu;
    private ImageView colorSelected;

    private static ArrayList<String> appList;
    private static Map<String, String> appLaunchList;


    FragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        globalState = (GlobalState) getApplication();
        topMenu = findViewById(R.id.settings_activity_menu);

        setupTopMenu();

        setTitle("Settings");

        //ViewPager uses the PagerAdapter class to make the different fragments.
        ViewPager vpPager = findViewById(R.id.vpPager);
        adapter = new PagerAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapter);

    }

    //the top menu is a layout of the color picker button, and a button to go back to the drawing screen.
    private void setupTopMenu() {
        Button menuDrawButton = topMenu.findViewById(R.id.top_menu_draw);
        Button menuColorPicker = topMenu.findViewById(R.id.top_menu_change_color);
        menuColorPicker.setText("Change color");
        menuDrawButton.setText("Drawing board");

        menuDrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        colorSelected = topMenu.findViewById(R.id.top_menu_color_selected);
        colorSelected.setColorFilter(Color.parseColor(globalState.getGestureColor()));

        menuColorPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showColorPicker();
            }
        });
        //as we have a button as well as an icon showing which color is currently in use
        //the color icon can also be clicked to chose color
        colorSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showColorPicker();
            }
        });
    }

    //The colorPicker utilizes a dialog to make a pop up of a wheel, from which a color can be selected.
    //Opacity and contrast is also possible to chose from
    private void showColorPicker() {
        context = SettingsActivity.this;
        ColorPickerDialogBuilder
                .with(context)
                .setTitle("Choose color")
                .initialColor(Color.parseColor(globalState.getGestureColor()))
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setOnColorSelectedListener(new OnColorSelectedListener() {
                    @Override
                    public void onColorSelected(int selectedColor) {
                        String gestureColor = "#"+Integer.toHexString(selectedColor);
                        Toast.makeText(context, "onColorSelected: " + gestureColor, Toast.LENGTH_SHORT).show();
                        globalState.storeGestureColor(gestureColor);
                    }
                })
                .setPositiveButton("ok", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                        colorSelected.setColorFilter(selectedColor);
                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .build()
                .show();
    }

}
