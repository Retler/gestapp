package com.example.sergej.gest_an_app;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Written by Oliver
 */
public class ShakeGesture implements SensorEventListener {
    private static final String TAG = "ShakeGesture";
    private static final int ACCELERATION_THRESHOLD = 13;
    private static final long SHAKE_THRESHHOLD = 2000000000L;
    private long lastShakeTime;

    public interface Listener {
        void hearShake();
    }

    private final SampleQueue queue = new SampleQueue();
    private final Listener listener;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    public ShakeGesture(Listener listener) {
        this.listener = listener;
    }

    public boolean start(SensorManager sensorManager) {
        if (mAccelerometer != null) {
            return true;
        }

        mAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // If this phone has an mAccelerometer, listen to it.
        if (mAccelerometer != null) {
            this.mSensorManager = sensorManager;
            sensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        }
        return mAccelerometer != null;
    }

    /**
     * Stops listening.  Safe to call when already stopped
     */
    public void stop() {
        if (mAccelerometer != null) {
            mSensorManager.unregisterListener(this, mAccelerometer);
            mSensorManager = null;
            mAccelerometer = null;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        boolean accelerating = isAccelerating(event);
        long timestamp = event.timestamp;
        queue.add(timestamp, accelerating);

        // Shakes should be 2 seconds apart
        Long allowNextShakeTime = lastShakeTime + SHAKE_THRESHHOLD;
        if (queue.isShaking() && allowNextShakeTime < timestamp) {
            lastShakeTime = timestamp;
            queue.clear();
            listener.hearShake();
        }
    }

    /**
     * Returns true if the device is currently accelerating
     */
    private boolean isAccelerating(SensorEvent event) {
        float ax = event.values[0];
        float ay = event.values[1];
        float az = event.values[2];

        // Instead of comparing magnitude to ACCELERATION_THRESHOLD,
        // compare their squares. This is equivalent and doesn't need the
        // actual magnitude, which would be computed using (expesive) Math.sqrt().
        final double magnitudeSquared = ax * ax + ay * ay + az * az;
        return magnitudeSquared > ACCELERATION_THRESHOLD * ACCELERATION_THRESHOLD;
    }

    /**
     * Queue of samples. Keeps a running average
     */
    static class SampleQueue {
        /**
         * Window size in ns. Used to compute the average
         */
        private static final long MAX_WINDOW_SIZE = 500000000; // 0.5s
        private static final long MIN_WINDOW_SIZE = MAX_WINDOW_SIZE >> 1; // 0.25s

        /**
         * Ensure the queue size never falls below this size, even if the device
         * fails to deliver this many events during the time window. The LG Ally
         * is one such device
         */

        private static final int MIN_QUEUE_SIZE = 4;

        private final SamplePool pool = new SamplePool();

        private Sample oldest;
        private Sample newest;
        private int sampleCount;
        private int acceleratingCount;

        /**
         * Adds a sample.
         *
         * @param timestamp    in nanoseconds of sample
         * @param accelerating true if > {@link #ACCELERATION_THRESHOLD}
         */
        void add(long timestamp, boolean accelerating) {
            // Purge samples that proceed window.
            purge(timestamp - MAX_WINDOW_SIZE);

            // Add the sample to the queue.
            Sample added = pool.acquire();
            added.timestamp = timestamp;
            added.accelerating = accelerating;
            added.next = null;
            if (newest != null) {
                newest.next = added;
            }
            newest = added;
            if (oldest == null) {
                oldest = added;
            }

            // Update running average.
            sampleCount++;
            if (accelerating) {
                acceleratingCount++;
            }
        }

        /**
         * Removes all samples from this queue
         */
        void clear() {
            while (oldest != null) {
                Sample removed = oldest;
                oldest = removed.next;
                pool.release(removed);
            }
            newest = null;
            sampleCount = 0;
            acceleratingCount = 0;
        }

        /**
         * Purges samples with timestamps older than cutoff
         */
        void purge(long cutoff) {
            while (sampleCount >= MIN_QUEUE_SIZE
                    && oldest != null && cutoff - oldest.timestamp > 0) {
                // Remove sample.
                Sample removed = oldest;
                if (removed.accelerating) {
                    acceleratingCount--;
                }
                sampleCount--;

                oldest = removed.next;
                if (oldest == null) {
                    newest = null;
                }
                pool.release(removed);
            }
        }

        /**
         * Returns true if we have enough samples and more than 3/4 of those samples
         * are accelerating
         */
        boolean isShaking() {
            return newest != null
                    && oldest != null
                    && newest.timestamp - oldest.timestamp >= MIN_WINDOW_SIZE
                    && acceleratingCount >= (sampleCount >> 1) + (sampleCount >> 2);
        }
    }

    /**
     * An mAccelerometer sample
     */
    static class Sample {
        /**
         * Time sample was taken
         */
        long timestamp;

        /**
         * If acceleration > {@link #ACCELERATION_THRESHOLD}
         */
        boolean accelerating;

        /**
         * Next sample in the queue or pool
         */
        Sample next;
    }

    /**
     * Pools samples. Avoids garbage collection
     */
    static class SamplePool {
        private Sample head;

        /**
         * Acquires a sample from the pool
         */
        Sample acquire() {
            Sample acquired = head;
            if (acquired == null) {
                acquired = new Sample();
            } else {
                // Remove instance from pool.
                head = acquired.next;
            }
            return acquired;
        }

        /**
         * Returns a sample to the pool
         */
        void release(Sample sample) {
            sample.next = head;
            head = sample;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // This implementation isn't needed for our purpose
    }
}
