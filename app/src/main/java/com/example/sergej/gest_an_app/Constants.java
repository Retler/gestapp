package com.example.sergej.gest_an_app;

//Constants for places where the same string is used multiple times, to reduce chance of typos
//and the case where we want to change the string, so we only have to do it one place.
public class Constants {
    public static final String APPS_CHOOSER = "More apps...";
    public static final String DEFAULT_ACTION = "No app chosen...";
}
